
import javax.swing.ImageIcon;
import javax.swing.JButton;


public class Data {

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public JButton getInfoBut() {
        return infoBut;
    }

    private String code;
    private String date;
    private String time;
    ImageIcon infoIcon = new ImageIcon("iconfinder_icon-ios7-information-outline_211761");
    private JButton infoBut = new JButton(infoIcon);

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
